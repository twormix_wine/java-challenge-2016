package org.twormix.exceptional;

public class TwormixException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String operation;
	private Object object;

	public TwormixException(String operation) {
		this.operation = operation;
		this.object = null;
	}

	public TwormixException(String operation, Object object) {
		super(operation);
		this.operation = operation;
		this.object = object;
	}
	
	public TwormixException(String operation, Throwable object) {
		super(operation, object);
		this.operation = operation;
		this.object = object;
	}

	public Object getObject() {
		return object;
	}

	public String getOperation() {
		return operation;
	}
	
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder("TwormixException with operation " + operation);
		if (object != null) {
			str.append(", object " + object);
		}
		if (getCause() != null) {
			str.append(", cause " + getCause());
		}
		return str.toString();
	}
}

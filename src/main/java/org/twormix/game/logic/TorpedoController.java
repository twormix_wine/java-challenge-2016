package org.twormix.game.logic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

import org.twormix.types.Circle;
import org.twormix.types.Coordinate;
import org.twormix.types.Entity;
import org.twormix.types.Game;
import org.twormix.types.ShootTorpedoRequest;
import org.twormix.types.Submarine;

public class TorpedoController {

	private Object checkTorpedoNextRoundCollision(Game game, Submarine sub, List<Entity> sonar, Coordinate start,
			Coordinate end) {

		Map<Object, Circle> check = new HashMap<>();

		for (Coordinate coord : game.getMapConfiguration().getIslandPositions()) {
			check.put(coord, new Circle(coord, game.getMapConfiguration().getIslandSize()));
		}
		for (Entity e : sonar) {
			if (e instanceof Submarine) {
				Submarine s = (Submarine) e;
				if (sub.getId() == s.getId()) // who started the torpedo shall
												// not be checked
					continue;
				check.put(e, new Circle(Coordinate.motion(s.getPosition(), s.getAngle(), s.getVelocity()),
						game.getMapConfiguration().getSubmarineSize()));
			}
		}

		Object min = null;
		double minColl = 0.0;

		for (Object o : check.keySet()) {
			Circle c = check.get(o);
			List<Coordinate> coords = Coordinate.lineCircleCollisionPoints(c.origin, c.radius, start, end);
			if (coords.size() > 0) {
				coords = coords.stream()
						.sorted((a, b) -> (Coordinate.distance(a, start) < Coordinate.distance(b, start) ? -1 : 1))
						.collect(Collectors.toList());

				Coordinate coll = coords.get(0);

				double distFromTorpedoStart = Coordinate.distance(coll, start);

				System.out.println(distFromTorpedoStart);

				if (min == null || minColl > distFromTorpedoStart) {
					minColl = distFromTorpedoStart;
					min = o;
				}
			}
		}

		return min;

	}

	public ShootTorpedoRequest getTorpedoRequest(Game game, Submarine sub, List<Entity> sonar) {

		if (sub.getTorpedoCooldown() != 0)
			return null;

		ShootTorpedoRequest torpedo = new ShootTorpedoRequest();
		torpedo.setGameId(game.getId());
		torpedo.setSubmarineId(sub.getId());
		int torpedoLife = game.getMapConfiguration().getTorpedoRange();
		double torpedoSpeed = game.getMapConfiguration().getTorpedoSpeed();
		double expRadii = game.getMapConfiguration().getTorpedoExplosionRadius();
		double subSize = game.getMapConfiguration().getSubmarineSize();

		List<Submarine> enemies = SonarFilter.filter(sonar, Submarine.class, new EnemyTeamPredicate());

		List<Submarine> allies = SonarFilter.filter(sonar, Submarine.class, new OwnTeamPredicate());

		Coordinate sp = sub.getPosition();

		Map<Submarine, Double> requests = new HashMap<>();

		if (!enemies.isEmpty()) {
			for (Submarine enemy : enemies) {
				for (int n = 1; n < torpedoLife; n++) {
					Coordinate futureEnemy = Coordinate.motion(enemy.getPosition(), enemy.getAngle(),
							n * enemy.getVelocity());
					double angle = Coordinate.calcAngle(sub.getPosition(), futureEnemy);
					Coordinate futureTorpedo = Coordinate.motion(sp, angle, n * torpedoSpeed);
					Coordinate finalTorpedo = futureTorpedo;
					if (Coordinate.distance(futureTorpedo, futureEnemy) < subSize) {
						Object coll = checkTorpedoNextRoundCollision(game, sub, sonar, sp,
								Coordinate.motion(sp, angle, torpedoSpeed));

						if (coll != null && coll instanceof Submarine) {
							Submarine scoll = (Submarine) coll;

							if ("twormix".equals(scoll.getOwner().getName().toLowerCase())) {
								continue;
							}
						}
						boolean shoot = true;
						for (Submarine ally : allies) {

							Coordinate fpos = Coordinate.motion(ally.getPosition(), ally.getAngle(),
									n * ally.getVelocity());

							if (Coordinate.distance(fpos, finalTorpedo) < expRadii + subSize) {
								shoot = false;
							}

							for (int k = 1; k <= n; k++) {
								fpos = Coordinate.motion(ally.getPosition(), ally.getAngle(), k * ally.getVelocity());
								futureTorpedo = Coordinate.motion(sp, angle, k * torpedoSpeed);
								double d = Coordinate.distance(futureTorpedo, fpos);
								if (d < subSize) {
									shoot = false;
								}
							}
						}
						if (shoot) {
							requests.put(enemy, angle);
						}
					}
				}
			}

			if (requests.isEmpty())
				return null;
			Submarine lastTarget = null;
			for (Submarine es : requests.keySet()) {
				if (!es.getOwner().getName().toLowerCase().contains("bot")) {
					System.out.println("Target: " + es);
					lastTarget = es;
					break;
				}
			}

			if (lastTarget == null) {
				Random rnd = new Random();
				lastTarget = new ArrayList<>(requests.keySet()).get(rnd.nextInt(requests.size()));
			}

			torpedo.setAngle(requests.get(lastTarget));
			torpedo.setTargetId(lastTarget.getId());

			System.out.println("Target: " + lastTarget);
			return torpedo;

		} else {
			return null;
		}
	}

}

package org.twormix.game.logic;

import java.awt.BorderLayout;
import java.awt.Color;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFrame;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.twormix.exceptional.TwormixException;
import org.twormix.game.controller.SubmarineGameController;
import org.twormix.game.controller.SubmarineHttpGameController;
import org.twormix.types.Entity;
import org.twormix.types.Game;
import org.twormix.types.MapConfiguration;
import org.twormix.types.json.EntityJsonConverter;
import org.twormix.types.json.EntityListJsonConverter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class WeLoveWine extends JFrame {

	private static final String TEAM_TOKEN = "EEBFE0A92241624E3C12758B568883C6";
	private static final Logger LOGGER = Logger.getLogger(WeLoveWine.class.getName());

	public static void main(String[] args) {

		String server = "http://195.228.45.100:8080/jc16-srv";
		if (args.length == 1) {
			server = args[0];
		}

		try {
			GsonBuilder builder = new GsonBuilder();
			builder.registerTypeAdapter(Entity.class, new EntityJsonConverter());
			builder.registerTypeAdapter(new ArrayList<Entity>().getClass(), new EntityListJsonConverter());
			Gson gson = builder.create();

			HttpClient http = HttpClientBuilder.create().build();
			SubmarineGameController controller = new SubmarineHttpGameController(server, TEAM_TOKEN, http, gson);
			GameStepLogic gameStep = new GameStepLogic(controller);
			GameLogic logic = new GameLogic(controller, gameStep);

			Game game = logic.startGameOrJoin();
			MapConfiguration config = game.getMapConfiguration();
			
			JFrame mainFrame = new JFrame();
			mainFrame.setLayout(new BorderLayout());
			mainFrame.setTitle("Twormix Wine");
			mainFrame.setSize(config.getWidth(), config.getHeight());
			mainFrame.getContentPane().setBackground(Color.blue);
			
			logic.roundLogic(mainFrame);

		} catch (TwormixException e) {
			LOGGER.log(Level.SEVERE, e.toString(), e);
		}
	}

}

package org.twormix.game.logic;

import java.util.function.Predicate;

import org.twormix.types.Submarine;

public class EnemyTeamPredicate implements Predicate<Submarine> {

	@Override
	public boolean test(Submarine t) {
		return !"twormix".equals(t.getOwner().getName().toLowerCase());		
	}

}

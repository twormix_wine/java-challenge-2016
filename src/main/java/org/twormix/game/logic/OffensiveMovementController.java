package org.twormix.game.logic;

import java.util.List;

import org.twormix.game.logic.MovementController.MovementControllerAdapterResponse;
import org.twormix.types.Coordinate;
import org.twormix.types.Entity;
import org.twormix.types.Game;
import org.twormix.types.MapConfiguration;
import org.twormix.types.Submarine;

public class OffensiveMovementController {

	public MovementControllerAdapterResponse getMotion(Game game, Submarine sub, List<Entity> sonar) {

		MapConfiguration config = game.getMapConfiguration();

		List<Submarine> enemies = SonarFilter.filter(sonar, Submarine.class, new EnemyTeamPredicate());

		if (enemies.isEmpty())
			return null;

		double minDist = Coordinate.distance(enemies.get(0).getPosition(), sub.getPosition());
		Submarine minDistEnemy = enemies.get(0);
		for (Submarine enemy : enemies) {

			double d = Coordinate.distance(sub.getPosition(), enemy.getPosition());

			if (d < minDist) {
				d = minDist;
				minDistEnemy = enemy;
			}
		}

		System.out.println(sub.getId() + " following " + minDistEnemy);

		Coordinate nextChosenPosition = Coordinate.motion(minDistEnemy.getPosition(), minDistEnemy.getAngle(),
				minDistEnemy.getVelocity());

		double distToChosen = Coordinate.distance(sub.getPosition(), nextChosenPosition);
		double subSize = game.getMapConfiguration().getSubmarineSize();
		double expRadii = game.getMapConfiguration().getTorpedoExplosionRadius();

		MovementControllerAdapterResponse resp = new MovementControllerAdapterResponse();

		if (distToChosen - 2 * subSize > expRadii) {

			double targetAngle = Coordinate.calcAngle(sub.getPosition(), nextChosenPosition);

			double deltaAngle = MovementController.getDeltaAngle(sub.getAngle(), targetAngle,
					game.getMapConfiguration().getMaxSteeringPerRound(), 0);

			resp.deltaAngle = deltaAngle;

			double speedForPath = distToChosen > expRadii + config.getMaxSpeed() ? config.getMaxSpeed()
					: minDistEnemy.getVelocity();

			double deltaSpeed = MovementController.getDeltaSpeed(sub.getVelocity(), speedForPath,
					game.getMapConfiguration().getMaxAccelerationPerRound(), game.getMapConfiguration().getMaxSpeed());

			resp.deltaSpeed = deltaSpeed;
		} else {

			double speedForPath = 0;
			resp.deltaSpeed = MovementController.getDeltaSpeed(sub.getVelocity(), speedForPath,
					game.getMapConfiguration().getMaxAccelerationPerRound(), game.getMapConfiguration().getMaxSpeed());
		}

		return resp;
	}
}

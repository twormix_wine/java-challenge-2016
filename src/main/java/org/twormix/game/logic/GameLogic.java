package org.twormix.game.logic;

import java.time.LocalTime;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFrame;

import org.twormix.exceptional.TwormixException;
import org.twormix.game.controller.SubmarineGameController;
import org.twormix.gui.MainGui;
import org.twormix.types.AbstractResponse;
import org.twormix.types.AbstractResponse.ErrorCode;
import org.twormix.types.CreateGameRequest;
import org.twormix.types.CreateGameResponse;
import org.twormix.types.Entity;
import org.twormix.types.Game;
import org.twormix.types.GameInfoRequest;
import org.twormix.types.GameInfoResponse;
import org.twormix.types.GameListRequest;
import org.twormix.types.GameListResponse;
import org.twormix.types.GetControlledSubmarinesRequest;
import org.twormix.types.GetControlledSubmarinesResponse;
import org.twormix.types.JoinGameRequest;
import org.twormix.types.JoinGameResponse;
import org.twormix.types.MapConfiguration;
import org.twormix.types.Submarine;
import org.twormix.types.Torpedo;

/**
 * 
 * JOIN, PROCESS
 * 
 */
public class GameLogic {

	private final SubmarineGameController controller;
	private final GameStepLogic gameStep;

	private Game game = null;
	private Integer round = null;
	private List<Submarine> submarines = null;

	private static final Logger LOGGER = Logger.getLogger(GameLogic.class.getName());

	private static final int RETRY_DELAY = 500;
	private static final int TURN_MAX_DELAY = 200;
	private static final int MAX_TRIES = 1000;

	private MainGui gui;

	static {
		LOGGER.setLevel(Level.ALL);
	}

	public GameLogic(SubmarineGameController controller, GameStepLogic gameStep) {
		this.controller = controller;
		this.gameStep = gameStep;
	}

	public Game startGameOrJoin() {

		Integer joinedGameId = null;
		int tries = 0;
		while (joinedGameId == null && tries < MAX_TRIES) {

			LOGGER.log(Level.INFO, tries + ". try to connect to a game...");

			Integer gameToJoin = null;
			GameListResponse resp = controller.getGameList(new GameListRequest());
			if (resp.getGames().isEmpty()) {
				LOGGER.log(Level.INFO, "No games waiting for us...creating a new one.");

				CreateGameResponse create = controller.createGame(new CreateGameRequest());
				gameToJoin = create.getId();
			} else {

				LOGGER.log(Level.INFO, resp.getGames().size() + " games waiting for us: " + resp.getGames());

				gameToJoin = resp.getGames().get(0); // TODO what to do
			}

			if (gameToJoin != null) {

				LOGGER.log(Level.INFO, "Trying to join game " + gameToJoin);

				JoinGameRequest req = new JoinGameRequest();
				req.setGameId(gameToJoin);
				JoinGameResponse joinResp = controller.joinGame(req);

				if (joinResp.getCode() != 0) {
					AbstractResponse.ErrorCode errcode = AbstractResponse.ErrorCode.parseErrorCode(joinResp.getCode());
					LOGGER.log(Level.INFO, errcode + ": " + joinResp.getMessage());

					if (errcode.equals(ErrorCode.GAME_IN_PROGRESS)) {

						LOGGER.log(Level.INFO, "Game " + gameToJoin + " is in progress.");

						joinedGameId = gameToJoin;
					} else {
						joinedGameId = null;
					}
				} else {
					joinedGameId = gameToJoin;
				}
			}

			if (joinedGameId == null) {
				sleep(RETRY_DELAY);
			}
			tries++;
		}

		if (joinedGameId != null) {

			GameInfoRequest req = new GameInfoRequest();
			req.setGameId(joinedGameId);
			GameInfoResponse resp = controller.gameInfo(req);
			if (resp.getCode() != 0) {
				game = null;
				throw new TwormixException("startGameOrJoin.game_info");
			} else {
				game = resp.getGame();
				LOGGER.log(Level.INFO, "Joined to game " + joinedGameId);

				System.out.println("Map width: " + game.getMapConfiguration().getWidth());
				System.out.println("Map height: " + game.getMapConfiguration().getHeight());
				System.out.println("Island size: " + game.getMapConfiguration().getIslandSize());
				System.out.println("Island positions: " + game.getMapConfiguration().getIslandPositions());
				System.out.println("Submarine size: " + game.getMapConfiguration().getSubmarineSize());
				System.out
						.println("Torpedo explosion radius: " + game.getMapConfiguration().getTorpedoExplosionRadius());
			}

		} else {
			LOGGER.log(Level.SEVERE, "Could not join to ANY game :(");
			throw new TwormixException("startGameOrJoin");
		}

		return game;
	}

	public Game roundLogic(JFrame mainFrame) {

		updateGameInfo();

		MapConfiguration config = game.getMapConfiguration();
		gui = new MainGui();
		gui.setMainGui(config.getIslandPositions(), config.getIslandSize(), config.getWidth(), config.getHeight());
		gui.setSubmarines(submarines, config.getSubmarineSize());
		gui.setSonar(config.getSonarRange(), config.getExtendedSonarRange());
		mainFrame.getContentPane().add(gui);
		mainFrame.setVisible(true);

		while (game.getRound() <= game.getMapConfiguration().getRounds()) {

			try {
			if (game.getStatus().equals(Game.STATUS_RUNNING)) {
				updateSubmarines();
				processGameStep(mainFrame);
			} else if (game.getStatus().equals(Game.STATUS_WAITING)) {
				// TODO Game.STATUS_WAITING
			} else if (game.getStatus().equals(Game.STATUS_ENDED)) {
				return game;
			}

			updateGameInfo();
			
			} catch (TwormixException e) {
				System.err.println(e);
			}

			sleep(Math.min(game.getMapConfiguration().getRoundLength() / 8, TURN_MAX_DELAY));

		}
		return game;
	}

	private void processGameStep(JFrame mainFrame) {
		if (round == null || round != game.getRound()) {
			round = game.getRound();

			System.out.println(LocalTime.now() + " - " + round + ". round. Max lenght is "
					+ game.getMapConfiguration().getRoundLength());

			logScores();

			List<Entity> ent = gameStep.processGameStep(game, submarines);
			
			List<Submarine> allSubmarines = SonarFilter.filter(ent, Submarine.class);
			List<Torpedo> torpedos = SonarFilter.filter(ent, Torpedo.class);
			
			MapConfiguration config = game.getMapConfiguration();
			gui.setMainGui(config.getIslandPositions(), config.getIslandSize(), config.getWidth(), config.getHeight());
			gui.setSubmarines(allSubmarines, config.getSubmarineSize());		
			gui.setTorpedos(torpedos);

			gui.repaint();
		}
	}

	private void updateGameInfo() {
		GameInfoRequest req = new GameInfoRequest();
		req.setGameId(game.getId());
		GameInfoResponse resp = controller.gameInfo(req);

		if (resp.getCode() != 0) {
			throw new TwormixException("updateGameInfo.fail", resp);
		} else {
			game = resp.getGame();
		}
	}

	private void updateSubmarines() {
		GetControlledSubmarinesRequest req = new GetControlledSubmarinesRequest();
		req.setGameId(game.getId());
		GetControlledSubmarinesResponse resp = controller.submarines(req);

		if (resp.getCode() != 0) {
			throw new TwormixException("updateSubmarines.fail", resp);
		} else {
			submarines = resp.getSubmarines();
		}
	}

	private void logScores() {
		System.out.println(game.getScores().getScores().entrySet().stream()
				.map(entry -> entry.getKey() + ": " + entry.getValue()).reduce("", (a, b) -> a + "\n" + b).trim());
	}

	private static void sleep(int time) {
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			System.err.println("wtf???");
		}
	}
}

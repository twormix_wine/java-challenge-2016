package org.twormix.game.logic;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.twormix.game.logic.MovementController.MovementControllerAdapterResponse;
import org.twormix.types.Coordinate;
import org.twormix.types.Entity;
import org.twormix.types.Game;
import org.twormix.types.MapConfiguration;
import org.twormix.types.Submarine;

public class FreeMovementController {
	private Map<Integer, Double> submarineAngles = new HashMap<>();

	public MovementControllerAdapterResponse getMotion(Game game, Submarine sub, List<Entity> sonar) {

		MovementControllerAdapterResponse movementResponse = new MovementControllerAdapterResponse();

		MapConfiguration config = game.getMapConfiguration();

		int subId = sub.getId();

		double speedTarget = config.getMaxSpeed();
		int direction = 0;

		Double angle = submarineAngles.get(subId);

		double w = config.getWidth();
		double h = config.getHeight();

		if (Math.random() * 1000 < 75) {
			angle = null;
		}

		List<Submarine> allies = SonarFilter.filter(sonar, Submarine.class, new OwnTeamPredicate(sub.getId()));

		if (!allies.isEmpty()) {
			Submarine closest = allies.get(0);
			double mind = Coordinate.distance(closest.getPosition(), sub.getPosition());
			for (Submarine s : allies) {
				double d = Coordinate.distance(s.getPosition(), sub.getPosition());
				if (d < mind) {
					mind = d;
					closest = s;
				}
			}

			double sonarRange = closest.getSonarExtended() > 0 ? config.getExtendedSonarRange()
					: config.getSonarRange();

			if (mind < sonarRange) {
				angle = Coordinate.normalizeDegrees(-closest.getAngle());
			}
		}

		if (angle == null) {

			if (sub.getPosition().x < w / 3 && sub.getPosition().y < h / 3) {

				angle = Math.random() * 90;

			} else if (sub.getPosition().x > 2 * w / 3 && sub.getPosition().y < h / 3) {

				angle = 90 + Math.random() * 90;

			} else if (sub.getPosition().x > 2 * w / 3 && sub.getPosition().y > 2 * h / 3) {

				angle = 180 + Math.random() * 90;

			} else if (sub.getPosition().x < w / 3 && sub.getPosition().y > 2 * h / 3) {

				angle = 270 + Math.random() * 90;

			} else if (sub.getPosition().x < w / 3) {

				angle = Coordinate.normalizeDegrees(-90 + Math.random() * 180);

			} else if (sub.getPosition().x > 2 * w / 3) {

				angle = 90 + Math.random() * 180;

			} else if (sub.getPosition().y > 2 * h / 3) {

				angle = 180 + Math.random() * 180;

			} else if (sub.getPosition().y < h / 3) {

				angle = Math.random() * 180;

			} else {

				angle = Math.random() * 360;
			}
		}

		submarineAngles.put(subId, angle);

		double deltaAngle = MovementController.getDeltaAngle(sub.getAngle(), submarineAngles.get(subId),
				game.getMapConfiguration().getMaxSteeringPerRound(), direction);

		double deltaSpeed = MovementController.getDeltaSpeed(sub.getVelocity(), speedTarget,
				game.getMapConfiguration().getMaxAccelerationPerRound(), game.getMapConfiguration().getMaxSpeed());

		movementResponse.deltaAngle = deltaAngle;
		movementResponse.deltaSpeed = deltaSpeed;

		return movementResponse;
	}
}
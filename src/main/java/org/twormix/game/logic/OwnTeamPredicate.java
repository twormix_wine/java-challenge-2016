package org.twormix.game.logic;

import java.util.function.Predicate;

import org.twormix.types.Submarine;;

public class OwnTeamPredicate implements Predicate<Submarine> {

	private Integer exclude = null;

	public OwnTeamPredicate() {
	}

	public OwnTeamPredicate(Integer ex) {
		exclude = ex;
	}

	@Override
	public boolean test(Submarine t) {
		boolean flag = true;
		if (exclude != null)
			flag = t.getId() != exclude;
		return "twormix".equals(t.getOwner().getName().toLowerCase()) && flag;
	}

}

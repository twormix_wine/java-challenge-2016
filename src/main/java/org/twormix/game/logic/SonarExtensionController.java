package org.twormix.game.logic;

import java.util.List;

import org.twormix.types.Coordinate;
import org.twormix.types.Entity;
import org.twormix.types.ExtendedSonarRequest;
import org.twormix.types.Game;
import org.twormix.types.Submarine;

public class SonarExtensionController {

	public ExtendedSonarRequest getSonar(Game game, Submarine sub, List<Entity> sonar) {

		if (sub.getSonarCooldown() != 0) {
			return null;
		}

		if (SonarFilter.filter(sonar, Submarine.class, new EnemyTeamPredicate()).isEmpty()) {

			if (!SonarFilter.filter(sonar, Submarine.class, new OwnTeamPredicate()).stream()
					.anyMatch(s -> s.getSonarExtended() > 0 && Coordinate.distance(s.getPosition(),
							sub.getPosition()) < game.getMapConfiguration().getSonarRange())) {

				ExtendedSonarRequest req = new ExtendedSonarRequest();
				req.setGameId(game.getId());
				req.setSubmarineId(sub.getId());
				return req;
			}
		}
		return null;

	}
}

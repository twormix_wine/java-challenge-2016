package org.twormix.game.logic;

import java.util.List;

import org.twormix.types.Coordinate;
import org.twormix.types.Entity;
import org.twormix.types.Game;
import org.twormix.types.MoveSubmarineRequest;
import org.twormix.types.Submarine;
import org.twormix.types.Torpedo;

public class MovementController {

	protected static class MovementControllerAdapterResponse {
		protected double deltaAngle;
		protected double deltaSpeed;
	}

	public enum MovementWarning {
		DELTA_ANGLE_TOO_BIG, DELTA_SPEED_TOO_BIG, OUT_OF_AREA, ISLAND_COLLISION, TORPEDO_COLLISION, TORPEDO_EXPLOSION, OK;
	}

	private OffensiveMovementController ofController = new OffensiveMovementController();
	private FreeMovementController rlController = new FreeMovementController();

	private MotionCrisisProtocol motionCrisisProtocol = new MotionCrisisProtocol();
	private TorpedoAvoiderMovementFilter torpedoProtocol = new TorpedoAvoiderMovementFilter();

	protected static double getDeltaSpeed(double curr, double target, double maxChange, double maxValue) {

		if (target > maxValue)
			target = maxValue;

		double delta = target - curr;

		if (Math.abs(delta) > maxChange)
			delta = Math.signum(delta) * maxChange;

		if (delta < maxChange) {
			delta = 0;
		}

		return delta;
	}

	// curr, target, maxChange between 0-360
	protected static double getDeltaAngle(double curr, double target, double maxChange, int direction) {
		double change = target - curr;

		if (change != 0) {
			if (direction < 0 && change > 0)
				change = change - 360;
			else if (direction > 0 && change < 0)
				change = change + 360;
			else if (direction == 0) {
				if (change > 180) {
					change = change - 360;
				} else if (change < -180) {
					change = change + 360;
				}
			}

			if (Math.abs(change) > maxChange) {
				change = Math.signum(change) * maxChange;
			}
		}
		return change;
	}

	public static double getMinDistanceFromWalls(Game game, Coordinate future) {
		double x = future.x;
		double y = future.y;
		double w = game.getMapConfiguration().getWidth();
		double h = game.getMapConfiguration().getHeight();
		return Math.min(Math.min(x, w - x), Math.min(y, h - y));
	}

	protected static MovementWarning futureRoundWarning(Game game, Submarine sub, List<Entity> sonar, double deltaAngle,
			double deltaVelocity) {

		// TODO process 2 steps in advance
		return MovementWarning.OK;
	}

	// avoid islands, map walls, torpedos
	protected static MovementWarning isMotionChangeOk(Game game, Submarine sub, List<Entity> sonar, double deltaAngle,
			double deltaVelocity) {

		if (Math.abs(deltaAngle) > game.getMapConfiguration().getMaxSteeringPerRound()) {
			return MovementWarning.DELTA_ANGLE_TOO_BIG;
		}
		if (Math.abs(deltaVelocity) > game.getMapConfiguration().getMaxAccelerationPerRound()) {
			return MovementWarning.DELTA_SPEED_TOO_BIG;
		}

		double angle = Coordinate.normalizeDegrees(sub.getAngle() + deltaAngle);
		double velocity = sub.getVelocity() + deltaVelocity;

		if (velocity < 0 || velocity > game.getMapConfiguration().getMaxSpeed()) {
			return MovementWarning.DELTA_SPEED_TOO_BIG;
		}

		Coordinate current = sub.getPosition();
		Coordinate newCoordinate = Coordinate.motion(current, angle, velocity);

		int width = game.getMapConfiguration().getWidth();
		int height = game.getMapConfiguration().getHeight();

		double subSize = game.getMapConfiguration().getSubmarineSize();
		double iSize = game.getMapConfiguration().getIslandSize();
		double expRadii = game.getMapConfiguration().getTorpedoExplosionRadius();

		double safeOffset = getSafeOffset(game);

		// map
		if ((newCoordinate.x - safeOffset) <= 0 || (newCoordinate.y - safeOffset) <= 0
				|| (newCoordinate.x + safeOffset) >= width || (newCoordinate.y + safeOffset) >= height) {
			return MovementWarning.OUT_OF_AREA;
		}

		// islands
		for (Coordinate ipos : game.getMapConfiguration().getIslandPositions()) {
			if (Coordinate.isLineCrossesCircle(ipos, expRadii + iSize + safeOffset, current, newCoordinate)) {
				return MovementWarning.ISLAND_COLLISION;
			}
		}

		// torpedo collision
		List<Torpedo> torpedosNear = SonarFilter.filter(sonar, Torpedo.class);
		List<Submarine> submarinesNear = SonarFilter.filter(sonar, Submarine.class);

		for (Torpedo t : torpedosNear) {
			Coordinate currentTorpedoCoord = t.getPosition();
			Coordinate newTorpedoCoord = Coordinate.motion(currentTorpedoCoord, t.getAngle(), t.getVelocity());
			if (Coordinate.isLineCrossesCircle(newCoordinate, subSize, currentTorpedoCoord, newTorpedoCoord)) {
				return MovementWarning.TORPEDO_COLLISION;
			}

			for (Submarine s : submarinesNear) {

				Coordinate newSCoord = Coordinate.motion(s.getPosition(), s.getAngle(), s.getVelocity());

				List<Coordinate> coords = Coordinate.lineCircleCollisionPoints(newSCoord, subSize, currentTorpedoCoord,
						newTorpedoCoord);
				if (coords.size() == 1) {
					if (Coordinate.distance(coords.get(0), newCoordinate) - subSize < game.getMapConfiguration()
							.getTorpedoExplosionRadius()) {
						return MovementWarning.TORPEDO_EXPLOSION;
					}
				} else if (coords.size() == 2) {

					double d1 = Coordinate.distance(coords.get(0), newCoordinate) - subSize;
					double d2 = Coordinate.distance(coords.get(1), newCoordinate) - subSize;

					double t1 = Coordinate.distance(coords.get(0), currentTorpedoCoord);
					double t2 = Coordinate.distance(coords.get(1), currentTorpedoCoord);

					if ((t1 < t2 ? d1 : d2) < game.getMapConfiguration().getTorpedoExplosionRadius()) {
						return MovementWarning.TORPEDO_EXPLOSION;
					}
				}
			}

			for (Coordinate ic : game.getMapConfiguration().getIslandPositions()) {
				List<Coordinate> coords = Coordinate.lineCircleCollisionPoints(ic, iSize, currentTorpedoCoord,
						newTorpedoCoord);
				if (coords.size() == 1) {
					if (Coordinate.distance(coords.get(0), newCoordinate) < game.getMapConfiguration()
							.getTorpedoExplosionRadius()) {
						return MovementWarning.TORPEDO_EXPLOSION;
					}
				} else if (coords.size() == 2) {

					double d1 = Coordinate.distance(coords.get(0), newCoordinate) - subSize;
					double d2 = Coordinate.distance(coords.get(1), newCoordinate) - subSize;

					double t1 = Coordinate.distance(coords.get(0), currentTorpedoCoord);
					double t2 = Coordinate.distance(coords.get(1), currentTorpedoCoord);

					if ((t1 < t2 ? d1 : d2) < game.getMapConfiguration().getTorpedoExplosionRadius()) {
						return MovementWarning.TORPEDO_EXPLOSION;
					}
				}
			}
		}

		return futureRoundWarning(game, sub, sonar, deltaAngle, deltaVelocity);
	}

	public static double getSafeOffset(Game game) {
		double maxSpeed = game.getMapConfiguration().getMaxSpeed();
		double maxAngle = game.getMapConfiguration().getMaxSteeringPerRound();

		double minAngleR = Math.toRadians((180 - maxAngle) / 2);
		double safeOffset = 0.0f;
		if (maxAngle == 0.0f) {
			// TODO
		} else {
			safeOffset = maxSpeed / (2 * Math.cos(minAngleR));
		}
		return safeOffset;
	}

	public MoveSubmarineRequest getMotion(Game game, Submarine sub, List<Entity> sonar) {
		MoveSubmarineRequest motion = new MoveSubmarineRequest();
		motion.setGameId(game.getId());
		motion.setSubmarineId(sub.getId());

		MovementControllerAdapterResponse resp = ofController.getMotion(game, sub, sonar);
		if (resp == null) {
			resp = rlController.getMotion(game, sub, sonar);
		}

		resp = torpedoProtocol.resolveCrisis(resp, game, sub, sonar);
		resp = motionCrisisProtocol.resolveCrisis(resp, game, sub, sonar);

		MovementWarning warn = isMotionChangeOk(game, sub, sonar, resp.deltaAngle, resp.deltaSpeed);

		System.out.println("Warning: " + warn);

		motion.setSpeed(resp.deltaSpeed);
		motion.setTurn(resp.deltaAngle);

		return motion;
	}

	public static double getMinDistanceFromIslands(Game game, Coordinate future) {

		if (game.getMapConfiguration().getIslandPositions().isEmpty()) {
			return Double.POSITIVE_INFINITY;
		}
		Coordinate start = game.getMapConfiguration().getIslandPositions().get(0);
		double minDist = Coordinate.distance(future, start);
		for (Coordinate ic : game.getMapConfiguration().getIslandPositions()) {

			double d = Coordinate.distance(future, ic);
			if (d < minDist) {
				minDist = d;
			}
		}
		return minDist;
	}

}

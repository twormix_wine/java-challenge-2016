package org.twormix.game.logic;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import org.twormix.types.Entity;

public class SonarFilter {
	
	@SuppressWarnings("unchecked")
	public static <T extends Entity> List<T> filter(List<Entity> entities, Class<T> clazz) {
		List<T> filtered = new ArrayList<>();
		for (Entity e : entities) {
			if (e.getClass().equals(clazz)) {
				filtered.add((T) e);
			}
		}
		return filtered;
	}
	
	@SuppressWarnings("unchecked")
	public static <T extends Entity> List<T> filter(List<Entity> entities, Class<T> clazz, Predicate<T> pred) {
		List<T> filtered = new ArrayList<>();
		for (Entity e : entities) {
			if (e.getClass().equals(clazz) && pred.test((T) e)) {
				filtered.add((T) e);
			}
		}
		return filtered;
	}
}

package org.twormix.game.logic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.twormix.game.controller.SubmarineGameController;
import org.twormix.types.Entity;
import org.twormix.types.ExtendedSonarRequest;
import org.twormix.types.ExtendedSonarResponse;
import org.twormix.types.Game;
import org.twormix.types.GameInfoRequest;
import org.twormix.types.MapConfiguration;
import org.twormix.types.MoveSubmarineRequest;
import org.twormix.types.MoveSubmarineResponse;
import org.twormix.types.PassiveSonarRequest;
import org.twormix.types.PassiveSonarResponse;
import org.twormix.types.ShootTorpedoRequest;
import org.twormix.types.ShootTorpedoResponse;
import org.twormix.types.Submarine;
import org.twormix.types.Torpedo;

/**
 * 
 * FREAKIN' LOGIC
 * 
 */
public class GameStepLogic {

	private final SubmarineGameController controller;

	private MovementController movement = new MovementController();
	private TorpedoController torpedo = new TorpedoController();
	private SonarExtensionController sonarController = new SonarExtensionController();

	public GameStepLogic(SubmarineGameController controller) {
		this.controller = controller;
	}

	public List<Entity> processGameStep(final Game game, final List<Submarine> submarines) {

		final int gameId = game.getId();

		Map<Integer, Entity> sonarMap = new HashMap<>();

		for (Submarine sub : submarines) {
			System.out.println("Submarine is at " + sub.getPosition() + ", speed is " + sub.getVelocity()
					+ ", angle is " + sub.getAngle() + ", health is " + sub.getHp());

			PassiveSonarRequest request = new PassiveSonarRequest();
			request.setGameId(gameId);
			request.setSubmarineId(sub.getId());
			PassiveSonarResponse sonarResponse = controller.sonar(request);
			if (sonarResponse != null && sonarResponse.getCode() == 0) {
				sonarMap.put(sub.getId(), sub);
				for (Entity e : sonarResponse.getEntities()) {
					sonarMap.put(e.getId(), e);
				}
			}
		}

		List<Entity> sonar = new ArrayList<>(sonarMap.values());

		for (Submarine sub : submarines) {
			ExtendedSonarRequest requestExt = sonarController.getSonar(game, sub, sonar);

			if (requestExt != null) {
				ExtendedSonarResponse respEx = controller.extendSonar(requestExt);

				if (respEx != null) {
					System.out.println("Sonar extension requested!");
					System.out.println("Result is \"" + respEx.getMessage() + "\"");
				}

				if (respEx != null && respEx.getCode() == 0) {
					sub.setSonarCooldown(game.getMapConfiguration().getExtendedSonarCooldown());
					sub.setSonarExtended(game.getMapConfiguration().getExtendedSonarRounds());
				}
			}
		}

		for (Submarine sub : submarines) {
			PassiveSonarRequest request = new PassiveSonarRequest();
			request.setGameId(gameId);
			request.setSubmarineId(sub.getId());
			PassiveSonarResponse sonarResponse = controller.sonar(request);
			if (sonarResponse != null && sonarResponse.getCode() == 0) {
				sonarMap.put(sub.getId(), sub);
				for (Entity e : sonarResponse.getEntities()) {
					sonarMap.put(e.getId(), e);
				}
			}
		}

		sonar = new ArrayList<>(sonarMap.values());

		for (Submarine sub : submarines) {

			ShootTorpedoRequest torpedoRequest = torpedo.getTorpedoRequest(game, sub, sonar);

			if (torpedoRequest != null) {
				ShootTorpedoResponse torpedoResponse = controller.shootTorpedo(torpedoRequest);
				System.out.println(
						"Torpedo launch requested. Result message is \"" + torpedoResponse.getMessage() + "\"");
				System.out.println("\tAngle is " + torpedoRequest.getAngle());
			}
		}

		sonar = new ArrayList<>(sonarMap.values());

		for (Submarine sub : submarines) {

			MoveSubmarineRequest movementRequest = movement.getMotion(game, sub, sonar);

			if (movementRequest != null) {
				MoveSubmarineResponse movementResponse = controller.moveSubmarine(movementRequest);
				System.out.println(
						"Submarine changed movement. Result message is \"" + movementResponse.getMessage() + "\"");
				System.out.println("\tSpeed delta is " + movementRequest.getSpeed());
				System.out.println("\tAngle delta is " + movementRequest.getTurn());
			}
		}

		return sonar;
	}
}

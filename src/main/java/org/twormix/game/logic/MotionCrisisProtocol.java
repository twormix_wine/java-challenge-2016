package org.twormix.game.logic;

import java.util.List;

import org.twormix.game.logic.MovementController.MovementControllerAdapterResponse;
import org.twormix.types.Coordinate;
import org.twormix.types.Entity;
import org.twormix.types.Game;
import org.twormix.types.MapConfiguration;
import org.twormix.types.Submarine;

public class MotionCrisisProtocol implements MovementAdapterFilter {

	public MovementControllerAdapterResponse resolveCrisis(MovementControllerAdapterResponse current, Game game,
			Submarine sub, List<Entity> sonar) {

		MapConfiguration config = game.getMapConfiguration();

		double safeOffset = MovementController.getSafeOffset(game);
		double islandSize = config.getIslandSize();
		double expRadii = config.getTorpedoExplosionRadius();
		double subSize = config.getSubmarineSize();

		Coordinate nextPosition = Coordinate.motion(sub.getPosition(), current.deltaAngle, sub.getVelocity());

		boolean isWallCollisionWarning = MovementController.getMinDistanceFromWalls(game, nextPosition) < safeOffset
				+ subSize;
		boolean isIsleCollisionWarning = MovementController.getMinDistanceFromIslands(game, nextPosition) < safeOffset
				+ islandSize + expRadii;

		if (!isWallCollisionWarning && !isIsleCollisionWarning)
			return current;

		MovementControllerAdapterResponse movementResponse = current;

		double angleTarget = sub.getAngle();

		if (isWallCollisionWarning) {

			Coordinate center = new Coordinate(config.getWidth() / 2, config.getHeight() / 2);
			if (((nextPosition.x < safeOffset) && (nextPosition.y < safeOffset))
					|| ((config.getWidth() - nextPosition.x < safeOffset)
							&& (config.getHeight() - nextPosition.y < safeOffset))
					|| ((nextPosition.x < safeOffset) && (config.getHeight() - nextPosition.y < safeOffset))
					|| ((config.getWidth() - nextPosition.x < safeOffset) && (nextPosition.y < safeOffset))) {

				angleTarget = Coordinate.calcAngle(sub.getPosition(), center);

			} else if (sub.getPosition().x < safeOffset + subSize) {
				angleTarget = 0;				
			} else if (config.getWidth() - sub.getPosition().x < safeOffset + subSize) {
				angleTarget = 180;
			} else if (sub.getPosition().y < safeOffset + subSize) {
				angleTarget = 90;
			} else if (config.getHeight() - sub.getPosition().y < safeOffset + subSize) {
				angleTarget = 270;
			}

		} else if (isIsleCollisionWarning) {

			List<Coordinate> ipos = config.getIslandPositions();
			Coordinate collIsland = config.getIslandPositions().get(0);
			Coordinate start = game.getMapConfiguration().getIslandPositions().get(0);
			double minDist = Coordinate.distance(nextPosition, start);
			for (Coordinate ic : ipos) {

				double d = Coordinate.distance(nextPosition, ic);
				if (d < minDist) {
					minDist = d;
					collIsland = ic;
				}
			}

			angleTarget = Coordinate.calcAngle(collIsland, sub.getPosition());

		}

		double diff = angleTarget - sub.getAngle();
		diff = Coordinate.normalizeDegrees(diff);
		diff = diff > 180 ? diff - 360 : diff;
		diff = Math.abs(diff);

		double deltaAngle = MovementController.getDeltaAngle(sub.getAngle(), angleTarget,
				game.getMapConfiguration().getMaxSteeringPerRound(), 0);

		double maxDeltaSpeed = config.getMaxAccelerationPerRound();

		double deltaSpeed = sub.getVelocity() > maxDeltaSpeed ? -maxDeltaSpeed : 0;

		if (diff < 45) {
			deltaSpeed = MovementController.getDeltaSpeed(sub.getVelocity(), config.getMaxSpeed(),
					config.getMaxAccelerationPerRound(), config.getMaxSpeed());
		}

		movementResponse.deltaSpeed = deltaSpeed;
		movementResponse.deltaAngle = deltaAngle;

		return movementResponse;
	}

}

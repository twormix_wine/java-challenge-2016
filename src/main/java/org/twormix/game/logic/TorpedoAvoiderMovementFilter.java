package org.twormix.game.logic;

import java.util.List;

import org.twormix.game.logic.MovementController.MovementControllerAdapterResponse;
import org.twormix.game.logic.MovementController.MovementWarning;
import org.twormix.types.Coordinate;
import org.twormix.types.Entity;
import org.twormix.types.Game;
import org.twormix.types.Submarine;
import org.twormix.types.Torpedo;

public class TorpedoAvoiderMovementFilter implements MovementAdapterFilter {

	private static boolean isTorpedoInMyPath(Game game, Submarine sub, Torpedo tor) {
		int maxRoundsMoved = game.getMapConfiguration().getTorpedoRange();
		int moved = tor.getRoundsMoved();
		Coordinate last = Coordinate.motion(tor.getPosition(), tor.getAngle(),
				(maxRoundsMoved - moved) * tor.getVelocity());

		for (int k = 1; k < maxRoundsMoved; k++) {

			Coordinate c = Coordinate.motion(sub.getPosition(), sub.getAngle(), k * sub.getVelocity());

			if (Coordinate.isLineCrossesCircle(c, game.getMapConfiguration().getSubmarineSize(), tor.getPosition(),
					last))
				return true;
		}

		return false;
	}

	private static double torpedoExplosionDistanceFuture(Game game, Submarine sub, Torpedo tor, double angle,
			double delta) {

		double newAngle = Coordinate.normalizeDegrees(sub.getAngle() + angle);
		double newSpeed = sub.getVelocity() + delta;

		Coordinate nextTorpedo = Coordinate.motion(tor.getPosition(), tor.getAngle(), tor.getVelocity());
		Coordinate nextMe = Coordinate.motion(sub.getPosition(), newAngle, newSpeed);

		return Coordinate.distance(nextTorpedo, nextMe);
	}

	public MovementControllerAdapterResponse resolveCrisis(MovementControllerAdapterResponse current, Game game,
			Submarine sub, List<Entity> sonar) {

		MovementControllerAdapterResponse newOne = new MovementControllerAdapterResponse();
		newOne.deltaSpeed = current.deltaSpeed;
		newOne.deltaAngle = current.deltaAngle;

		List<Torpedo> myTorpedos = SonarFilter.filter(sonar, Torpedo.class);

		boolean torpedoWarning = false;

		for (Torpedo t : myTorpedos) {
			if (torpedoExplosionDistanceFuture(game, sub, t, current.deltaAngle, current.deltaSpeed) < game
					.getMapConfiguration().getTorpedoExplosionRadius()) {
				torpedoWarning = true;
				break;
			}
		}

		if (!torpedoWarning)
			return newOne;

		double[] deltaSpeed = new double[2];

		deltaSpeed[0] = MovementController.getDeltaSpeed(sub.getVelocity(), 0,
				game.getMapConfiguration().getMaxAccelerationPerRound(), game.getMapConfiguration().getMaxSpeed());
		deltaSpeed[1] = MovementController.getDeltaSpeed(sub.getVelocity(), game.getMapConfiguration().getMaxSpeed(),
				game.getMapConfiguration().getMaxAccelerationPerRound(), game.getMapConfiguration().getMaxSpeed());
		double deltaAngleToCheck = game.getMapConfiguration().getMaxSteeringPerRound() / 10;
		int numDeltas = (int) Math.abs(game.getMapConfiguration().getMaxSteeringPerRound() / deltaAngleToCheck);

		double[] angles = new double[numDeltas * 2 + 1];

		for (int i = 0; i < angles.length; i++) {
			angles[i] = Coordinate.normalizeDegrees(
					sub.getAngle() - game.getMapConfiguration().getMaxSteeringPerRound() + deltaAngleToCheck * i);
			angles[i] = MovementController.getDeltaAngle(sub.getAngle(), angles[i],
					game.getMapConfiguration().getMaxSteeringPerRound(), 0);
		}

		double maxDist = 0;
		double bestAngle = newOne.deltaAngle;
		double bestSpeed = newOne.deltaSpeed;

		for (double angle : angles) {
			for (double delta : deltaSpeed) {
				double dsum = 0;
				for (Torpedo t : myTorpedos) {
					double d = torpedoExplosionDistanceFuture(game, sub, t, angle, delta);
					if (d < game.getMapConfiguration().getTorpedoExplosionRadius()) {
						dsum += d;
					}
				}
				if (dsum > maxDist) {
					maxDist = dsum;
					bestAngle = angle;
					bestSpeed = delta;
				}
			}
		}

		newOne.deltaAngle = bestAngle;
		newOne.deltaSpeed = bestSpeed;

		return newOne;
	}
}

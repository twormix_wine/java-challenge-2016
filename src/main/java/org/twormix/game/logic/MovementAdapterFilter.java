package org.twormix.game.logic;

import java.util.List;

import org.twormix.game.logic.MovementController.MovementControllerAdapterResponse;
import org.twormix.types.Entity;
import org.twormix.types.Game;
import org.twormix.types.Submarine;

public interface MovementAdapterFilter {

	// resolves all bad things that could happen. or not.
	MovementControllerAdapterResponse resolveCrisis(MovementControllerAdapterResponse current, Game game,
			Submarine sub, List<Entity> sonar);
}

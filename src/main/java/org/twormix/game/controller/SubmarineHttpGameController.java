package org.twormix.game.controller;

import java.io.IOException;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.twormix.exceptional.TwormixException;
import org.twormix.types.CreateGameRequest;
import org.twormix.types.CreateGameResponse;
import org.twormix.types.ExtendedSonarRequest;
import org.twormix.types.ExtendedSonarResponse;
import org.twormix.types.GameInfoRequest;
import org.twormix.types.GameInfoResponse;
import org.twormix.types.GameListRequest;
import org.twormix.types.GameListResponse;
import org.twormix.types.GetControlledSubmarinesRequest;
import org.twormix.types.GetControlledSubmarinesResponse;
import org.twormix.types.JoinGameRequest;
import org.twormix.types.JoinGameResponse;
import org.twormix.types.MoveSubmarineRequest;
import org.twormix.types.MoveSubmarineResponse;
import org.twormix.types.PassiveSonarRequest;
import org.twormix.types.PassiveSonarResponse;
import org.twormix.types.ShootTorpedoRequest;
import org.twormix.types.ShootTorpedoResponse;

import com.google.gson.Gson;

public class SubmarineHttpGameController implements SubmarineGameController {

	private String serverUrl;
	private String teamToken;
	private Gson gson;
	private HttpClient client;

	private static final String REQUEST_GET = "GET";
	private static final String REQUEST_POST = "POST";

	private static final String TEAM_HEADER = "TEAMTOKEN";

	private static final String JSON_HEADER = "content-type";
	private static final String JSON_HEADER_VALUE = "application/json";

	private static final Logger LOGGER = Logger.getLogger(SubmarineHttpGameController.class.getName());

	{
		LOGGER.setLevel(Level.ALL);
	}

	public SubmarineHttpGameController(String serverUrl, String teamToken, HttpClient client, Gson gson) {
		this.serverUrl = serverUrl;
		this.teamToken = teamToken;
		this.client = client;
		this.gson = gson;
	}

	private String httpRequest(String method, String urlString, String json) {
		try {
			HttpUriRequest request = null;
			if (REQUEST_GET.equals(method)) {
				request = new HttpGet(urlString);
			} else if (REQUEST_POST.equals(method)) {
				request = new HttpPost(urlString);
				if (json != null) {
					((HttpPost) request).setEntity(new StringEntity(json));
					request.addHeader(JSON_HEADER, JSON_HEADER_VALUE);
				}
			} else {
				throw new TwormixException("httpRequest.bad_method." + urlString + "." + method);
			}
			request.addHeader(TEAM_HEADER, teamToken);
			HttpResponse resp = client.execute(request);

			String jsonString = EntityUtils.toString(resp.getEntity());

			int statusCode = resp.getStatusLine().getStatusCode();

			LOGGER.log(statusCode == 200 ? Level.FINEST : Level.WARNING,
					"HTTP " + method + " request sent to " + urlString + (json == null ? "" : " with json " + json)
							+ (jsonString == null ? "" : ", response is " + jsonString) + " , return code is "
							+ statusCode);
			
			String entType = resp.getEntity().getContentType().getValue().toLowerCase();
			
			if (jsonString != null && entType != null && entType.contains(JSON_HEADER_VALUE)) // TODO must?
				return jsonString;
			
			if (jsonString != null) // TODO check json format
				return jsonString;
			
			throw new TwormixException("httpRequest.not_a_json_response", entType);

		} catch (IOException e) {
			throw new TwormixException("httpRequest.io_exception", e);
		}
	}

	@Override
	public CreateGameResponse createGame(CreateGameRequest request) throws TwormixException {
		LOGGER.log(Level.FINEST, "createGame");
		String respJson = httpRequest(REQUEST_POST, serverUrl.concat("/game"), null);
		CreateGameResponse resp = gson.fromJson(respJson, CreateGameResponse.class);
		return resp;
	}

	@Override
	public GameListResponse getGameList(GameListRequest request) throws TwormixException {
		LOGGER.log(Level.FINEST, "getGameList");
		String respJson = httpRequest(REQUEST_GET, serverUrl.concat("/game"), null);
		GameListResponse resp = gson.fromJson(respJson, GameListResponse.class);
		return resp;
	}

	@Override
	public JoinGameResponse joinGame(JoinGameRequest request) throws TwormixException {
		LOGGER.log(Level.FINEST, "joinGame");
		String respJson = httpRequest(REQUEST_POST, serverUrl.concat("/game/" + request.getGameId()), null);
		JoinGameResponse resp = gson.fromJson(respJson, JoinGameResponse.class);
		return resp;
	}

	@Override
	public GameInfoResponse gameInfo(GameInfoRequest request) throws TwormixException {
		LOGGER.log(Level.FINEST, "gameInfo");
		String respJson = httpRequest(REQUEST_GET, serverUrl.concat("/game/" + request.getGameId()), null);
		GameInfoResponse resp = gson.fromJson(respJson, GameInfoResponse.class);
		return resp;
	}

	@Override
	public GetControlledSubmarinesResponse submarines(GetControlledSubmarinesRequest request) throws TwormixException {
		LOGGER.log(Level.FINEST, "submarines");
		String respJson = httpRequest(REQUEST_GET,
				serverUrl.concat("/game/" + request.getGameId()).concat("/submarine"), null);
		GetControlledSubmarinesResponse resp = gson.fromJson(respJson, GetControlledSubmarinesResponse.class);
		return resp;
	}

	@Override
	public MoveSubmarineResponse moveSubmarine(MoveSubmarineRequest request) throws TwormixException {
		LOGGER.log(Level.FINEST, "moveSubmarine");
		String reqJson = gson.toJson(request);
		String respJson = httpRequest(REQUEST_POST, serverUrl.concat("/game/" + request.getGameId())
				.concat("/submarine/" + request.getSubmarineId()).concat("/move"), reqJson);
		MoveSubmarineResponse resp = gson.fromJson(respJson, MoveSubmarineResponse.class);
		return resp;
	}

	@Override
	public ShootTorpedoResponse shootTorpedo(ShootTorpedoRequest request) throws TwormixException {
		LOGGER.log(Level.FINEST, "shootTorpedo");
		String reqJson = gson.toJson(request);
		String respJson = httpRequest(REQUEST_POST, serverUrl.concat("/game/" + request.getGameId())
				.concat("/submarine/" + request.getSubmarineId()).concat("/shoot"), reqJson);
		ShootTorpedoResponse resp = gson.fromJson(respJson, ShootTorpedoResponse.class);
		return resp;
	}

	@Override
	public PassiveSonarResponse sonar(PassiveSonarRequest request) throws TwormixException {
		LOGGER.log(Level.FINEST, "sonar");
		String respJson = httpRequest(REQUEST_GET, serverUrl.concat("/game/" + request.getGameId())
				.concat("/submarine/" + request.getSubmarineId()).concat("/sonar"), null);
		PassiveSonarResponse resp = gson.fromJson(respJson, PassiveSonarResponse.class);
		return resp;
	}

	@Override
	public ExtendedSonarResponse extendSonar(ExtendedSonarRequest request) throws TwormixException {
		LOGGER.log(Level.FINEST, "extendSonar");
		String respJson = httpRequest(REQUEST_POST, serverUrl.concat("/game/" + request.getGameId())
				.concat("/submarine/" + request.getSubmarineId()).concat("/sonar"), null);
		ExtendedSonarResponse resp = gson.fromJson(respJson, ExtendedSonarResponse.class);
		return resp;
	}

}

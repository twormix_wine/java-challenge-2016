package org.twormix.game.controller;

import org.twormix.exceptional.TwormixException;
import org.twormix.types.AbstractResponse.ErrorCode;
import org.twormix.types.CreateGameRequest;
import org.twormix.types.CreateGameResponse;
import org.twormix.types.ExtendedSonarRequest;
import org.twormix.types.ExtendedSonarResponse;
import org.twormix.types.GameInfoRequest;
import org.twormix.types.GameInfoResponse;
import org.twormix.types.GameListRequest;
import org.twormix.types.GameListResponse;
import org.twormix.types.GetControlledSubmarinesRequest;
import org.twormix.types.GetControlledSubmarinesResponse;
import org.twormix.types.JoinGameRequest;
import org.twormix.types.JoinGameResponse;
import org.twormix.types.MoveSubmarineRequest;
import org.twormix.types.MoveSubmarineResponse;
import org.twormix.types.PassiveSonarRequest;
import org.twormix.types.PassiveSonarResponse;
import org.twormix.types.ShootTorpedoRequest;
import org.twormix.types.ShootTorpedoResponse;

public interface SubmarineGameController {

	/**
	 * Error codes: -
	 * @param request
	 * @return {@link CreateGameResponse}e
	 * @throws TwormixException
	 */
	CreateGameResponse createGame(CreateGameRequest request) throws TwormixException;

	/**
	 * Error codes: -
	 * @param request
	 * @return {@link GameListResponse}
	 * @throws TwormixException
	 */
	GameListResponse getGameList(GameListRequest request) throws TwormixException;

	/**
	 * {@link ErrorCode}:
	 * GROUP_NOT_INVITED, 
	 * GAME_IN_PROGRESS, 
	 * NONEXISTENT_GAMEID,
	 * @param request
	 * @return {@link JoinGameResponse}
	 * @throws TwormixException
	 */
	JoinGameResponse joinGame(JoinGameRequest request) throws TwormixException;

	/**
	 * {@link ErrorCode}:
	 * NONEXISTENT_GAMEID
	 * @param request
	 * @return {@link GameInfoResponse}
	 * @throws TwormixException
	 */
	GameInfoResponse gameInfo(GameInfoRequest request) throws TwormixException;

	/**
	 * {@link ErrorCode}:
	 * NONEXISTENT_GAMEID
	 * @param request
	 * @return {@link GetControlledSubmarinesResponse}
	 * @throws TwormixException
	 */
	GetControlledSubmarinesResponse submarines(GetControlledSubmarinesRequest request) throws TwormixException;
	
	/**
	 * {@link ErrorCode}:
	 * NONEXISTENT_GAMEID,
	 * NO_RIGHT_TO_CONTROL_SUBMARINE,
	 * GAME_IS_NOT_IN_PROGRESS,
	 * SUBMARINE_ALREADY_MOVED,
	 * TOO_MUCH_SPEED,
	 * TOO_MUCH_TURN
	 * @param request
	 * @return {@link MoveSubmarineResponse}
	 * @throws TwormixException
	 */
	MoveSubmarineResponse moveSubmarine(MoveSubmarineRequest request) throws TwormixException;
	
	/**
	 * {@link ErrorCode}:
	 * NONEXISTENT_GAMEID,
	 * NO_RIGHT_TO_CONTROL_SUBMARINE,
	 * TORPEDO_ON_COOLDOWN
	 * @param request
	 * @return {@link ShootTorpedoResponse}
	 * @throws TwormixException
	 */
	ShootTorpedoResponse shootTorpedo(ShootTorpedoRequest request) throws TwormixException;
	
	/**
	 * {@link ErrorCode}:
	 * NONEXISTENT_GAMEID,
	 * NO_RIGHT_TO_CONTROL
	 * @param request
	 * @return {@link PassiveSonarResponse}
	 * @throws TwormixException
	 */
	PassiveSonarResponse sonar(PassiveSonarRequest request) throws TwormixException;
	
	/**
	 * {@link ErrorCode}:
	 * NONEXISTENT_GAMEID,
	 * NO_RIGHT_TO_CONTROL,
	 * EXTENDED_SONAR_RECHARGING
	 * @param request
	 * @return {@link ExtendedSonarResponse}
	 * @throws TwormixException
	 */
	ExtendedSonarResponse extendSonar(ExtendedSonarRequest request) throws TwormixException;
}

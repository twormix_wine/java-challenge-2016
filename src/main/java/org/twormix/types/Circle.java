package org.twormix.types;

public class Circle {
	
	public Coordinate origin;
	public double radius;
	
	public Circle(Coordinate origin, double radius) {
		this.origin = origin;
		this.radius = radius;
	}
}

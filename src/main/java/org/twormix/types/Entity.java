package org.twormix.types;

public class Entity {

	public static final String TYPE_TORPEDO = "Torpedo";
	public static final String TYPE_SUBMARINE = "Submarine";

	private String type;
	private int id;
	private Coordinate position;
	private Team owner;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Coordinate getPosition() {
		return position;
	}

	public void setPosition(Coordinate position) {
		this.position = position;
	}

	public Team getOwner() {
		return owner;
	}

	public void setOwner(Team owner) {
		this.owner = owner;
	}

	@Override
	public String toString() {
		return "Entity [type=" + type + ", id=" + id + ", position=" + position + ", owner=" + owner + "]";
	}
}

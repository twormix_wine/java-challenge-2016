package org.twormix.types.json;

import java.lang.reflect.Type;

import org.twormix.types.Entity;
import org.twormix.types.Submarine;
import org.twormix.types.Torpedo;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class EntityJsonConverter implements JsonSerializer<Entity>, JsonDeserializer<Entity> {

	private static final String TYPE_FIELD = "type";

	@Override
	public Entity deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
			throws JsonParseException {
		
		final JsonObject jsonObject = json.getAsJsonObject();
		final String type = jsonObject.get(TYPE_FIELD).getAsString();

		if (Entity.TYPE_SUBMARINE.equals(type)) {
			
			return new Gson().fromJson(jsonObject, Submarine.class);
			
		} else if (Entity.TYPE_TORPEDO.equals(type)) {

			return new Gson().fromJson(jsonObject, Torpedo.class);
			
		} else {
			throw new JsonParseException(type + " is not a valid class for Entity");
		}
	}

	@Override
	public JsonElement serialize(Entity src, Type typeOfSrc, JsonSerializationContext context) {
		return new Gson().toJsonTree(src, src.getClass());
	}

}
package org.twormix.types.json;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.twormix.types.Entity;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class EntityListJsonConverter implements JsonSerializer<List<Entity>>, JsonDeserializer<List<Entity>> {

	@Override
	public List<Entity> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
			throws JsonParseException {
		if (json.isJsonArray()) {
			List<Entity> list = new ArrayList<Entity>();
			final JsonArray array = json.getAsJsonArray();
			for (int i = 0; i < array.size(); i++) {
				list.add(context.deserialize(array.get(i), Entity.class));
			}
			return list;
		} else {
			throw new JsonParseException(json + " is not a List<Entity>");
		}
	}

	@Override
	public JsonElement serialize(List<Entity> src, Type typeOfSrc, JsonSerializationContext context) {
		JsonArray array = new JsonArray();
		for (Entity e : src) {
			array.add(context.serialize(e));
		}
		return array;
	}

}

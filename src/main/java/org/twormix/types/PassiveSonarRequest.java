package org.twormix.types;

public class PassiveSonarRequest extends AbstractGameRequest {

	private transient int submarineId;

	public int getSubmarineId() {
		return submarineId;
	}

	public void setSubmarineId(int submarineId) {
		this.submarineId = submarineId;
	}
}

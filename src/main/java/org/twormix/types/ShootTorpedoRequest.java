package org.twormix.types;

public class ShootTorpedoRequest extends AbstractGameRequest {

	private transient int targetId;
	
	private transient int submarineId;
	
	private double angle;

	public double getAngle() {
		return angle;
	}

	public void setAngle(double angle) {
		this.angle = angle;
	}

	public int getSubmarineId() {
		return submarineId;
	}

	public void setSubmarineId(int submarineId) {
		this.submarineId = submarineId;
	}

	public int getTargetId() {
		return targetId;
	}
	
	public void setTargetId(int targetId) {
		this.targetId = targetId;
	}
}

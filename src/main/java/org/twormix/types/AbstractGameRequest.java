package org.twormix.types;

public abstract class AbstractGameRequest {
	
	// nem sz�rializ�land� a requestben json-k�nt, url-ben k�ldj�k
	private transient int gameId;

	public int getGameId() {
		return gameId;
	}

	public void setGameId(int gameId) {
		this.gameId = gameId;
	}
	
}

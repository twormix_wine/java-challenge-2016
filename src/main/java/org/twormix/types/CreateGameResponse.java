package org.twormix.types;

public class CreateGameResponse extends AbstractResponse {

	private int id;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
}

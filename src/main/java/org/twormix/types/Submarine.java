package org.twormix.types;

public class Submarine extends Entity {
	
	private double velocity;
	private double angle;
	private int hp;
	private int sonarCooldown;
	private int torpedoCooldown;
	private int sonarExtended;
	
	public double getVelocity() {
		return velocity;
	}
	public void setVelocity(double velocity) {
		this.velocity = velocity;
	}
	public double getAngle() {
		return angle;
	}
	public void setAngle(double angle) {
		this.angle = angle;
	}
	public int getHp() {
		return hp;
	}
	public void setHp(int hp) {
		this.hp = hp;
	}
	public int getSonarCooldown() {
		return sonarCooldown;
	}
	public void setSonarCooldown(int sonarCooldown) {
		this.sonarCooldown = sonarCooldown;
	}
	public int getTorpedoCooldown() {
		return torpedoCooldown;
	}
	public void setTorpedoCooldown(int torpedoCooldown) {
		this.torpedoCooldown = torpedoCooldown;
	}
	public int getSonarExtended() {
		return sonarExtended;
	}
	public void setSonarExtended(int sonarExtended) {
		this.sonarExtended = sonarExtended;
	}
	@Override
	public String toString() {
		return "Submarine [velocity=" + velocity + ", angle=" + angle + ", hp=" + hp + ", sonarCooldown="
				+ sonarCooldown + ", torpedoCooldown=" + torpedoCooldown + ", sonarExtended=" + sonarExtended
				+ ", type=" + getType() + ", id()=" + getId() + ", position()=" + getPosition()
				+ ", owner()=" + getOwner() + "]";
	}


}

package org.twormix.types;

public class Torpedo extends Entity {
	private double velocity;
	private double angle;
	private int roundsMoved;

	public double getVelocity() {
		return velocity;
	}

	public void setVelocity(int velocity) {
		this.velocity = velocity;
	}

	public double getAngle() {
		return angle;
	}

	public void setAngle(double angle) {
		this.angle = angle;
	}

	public int getRoundsMoved() {
		return roundsMoved;
	}

	public void setRoundsMoved(int roundsMoved) {
		this.roundsMoved = roundsMoved;
	}

	@Override
	public String toString() {
		return "Torpedo [velocity=" + velocity + ", angle=" + angle + ", position=" + getPosition() + "]";
	}
}

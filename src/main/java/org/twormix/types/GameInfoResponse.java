package org.twormix.types;

public class GameInfoResponse extends AbstractResponse {
	
	private Game game;

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}
}

package org.twormix.types;

import java.util.List;

public class MapConfiguration {
	private int width;
	private int height;
	private List<Coordinate> islandPositions;
	private int teamCount;
	private int submarinesPerTeam;
	private int torpedoDamage;
	private int torpedoHitScore;
	private int torpedoDestroyScore;
	private int torpedoHitPenalty;
	private int torpedoCooldown;
	private double sonarRange;
	private double extendedSonarRange;
	private int extendedSonarRounds;
	private int extendedSonarCooldown;
	private double torpedoSpeed;
	private double torpedoExplosionRadius;
	private int roundLength;
	private double islandSize;
	private double submarineSize;
	private int rounds;
	private double maxSteeringPerRound;
	private double maxAccelerationPerRound;
	private double maxSpeed;
	private int torpedoRange;
	private int rateLimitedPenalty;

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public List<Coordinate> getIslandPositions() {
		return islandPositions;
	}

	public void setIslandPositions(List<Coordinate> islandPositions) {
		this.islandPositions = islandPositions;
	}

	public int getTeamCount() {
		return teamCount;
	}

	public void setTeamCount(int teamCount) {
		this.teamCount = teamCount;
	}

	public int getSubmarinesPerTeam() {
		return submarinesPerTeam;
	}

	public void setSubmarinesPerTeam(int submarinesPerTeam) {
		this.submarinesPerTeam = submarinesPerTeam;
	}

	public int getTorpedoDamage() {
		return torpedoDamage;
	}

	public void setTorpedoDamage(int torpedoDamage) {
		this.torpedoDamage = torpedoDamage;
	}

	public int getTorpedoHitScore() {
		return torpedoHitScore;
	}

	public void setTorpedoHitScore(int torpedoHitScore) {
		this.torpedoHitScore = torpedoHitScore;
	}

	public int getTorpedoDestroyScore() {
		return torpedoDestroyScore;
	}

	public void setTorpedoDestroyScore(int torpedoDestroyScore) {
		this.torpedoDestroyScore = torpedoDestroyScore;
	}

	public int getTorpedoHitPenalty() {
		return torpedoHitPenalty;
	}

	public void setTorpedoHitPenalty(int torpedoHitPenalty) {
		this.torpedoHitPenalty = torpedoHitPenalty;
	}

	public int getTorpedoCooldown() {
		return torpedoCooldown;
	}

	public void setTorpedoCooldown(int torpedoCooldown) {
		this.torpedoCooldown = torpedoCooldown;
	}

	public double getSonarRange() {
		return sonarRange;
	}

	public void setSonarRange(double sonarRange) {
		this.sonarRange = sonarRange;
	}

	public double getExtendedSonarRange() {
		return extendedSonarRange;
	}

	public void setExtendedSonarRange(double extendedSonarRange) {
		this.extendedSonarRange = extendedSonarRange;
	}

	public int getExtendedSonarRounds() {
		return extendedSonarRounds;
	}

	public void setExtendedSonarRounds(int extendedSonarRounds) {
		this.extendedSonarRounds = extendedSonarRounds;
	}

	public int getExtendedSonarCooldown() {
		return extendedSonarCooldown;
	}

	public void setExtendedSonarCooldown(int extendedSonarCooldown) {
		this.extendedSonarCooldown = extendedSonarCooldown;
	}

	public double getTorpedoSpeed() {
		return torpedoSpeed;
	}

	public void setTorpedoSpeed(double torpedoSpeed) {
		this.torpedoSpeed = torpedoSpeed;
	}

	public double getTorpedoExplosionRadius() {
		return torpedoExplosionRadius;
	}

	public void setTorpedoExplosionRadius(double torpedoExplosionRadius) {
		this.torpedoExplosionRadius = torpedoExplosionRadius;
	}

	public int getRoundLength() {
		return roundLength;
	}

	public void setRoundLength(int roundLength) {
		this.roundLength = roundLength;
	}

	public double getIslandSize() {
		return islandSize;
	}

	public void setIslandSize(double islandSize) {
		this.islandSize = islandSize;
	}

	public double getSubmarineSize() {
		return submarineSize;
	}

	public void setSubmarineSize(double submarineSize) {
		this.submarineSize = submarineSize;
	}

	public int getRounds() {
		return rounds;
	}

	public void setRounds(int rounds) {
		this.rounds = rounds;
	}

	public double getMaxSteeringPerRound() {
		return maxSteeringPerRound;
	}

	public void setMaxSteeringPerRound(double maxSteeringPerRound) {
		this.maxSteeringPerRound = maxSteeringPerRound;
	}

	public double getMaxAccelerationPerRound() {
		return maxAccelerationPerRound;
	}

	public void setMaxAccelerationPerRound(double maxAccelerationPerRound) {
		this.maxAccelerationPerRound = maxAccelerationPerRound;
	}

	public double getMaxSpeed() {
		return maxSpeed;
	}

	public void setMaxSpeed(double maxSpeed) {
		this.maxSpeed = maxSpeed;
	}

	public int getTorpedoRange() {
		return torpedoRange;
	}

	public void setTorpedoRange(int torpedoRange) {
		this.torpedoRange = torpedoRange;
	}

	public int getRateLimitedPenalty() {
		return rateLimitedPenalty;
	}

	public void setRateLimitedPenalty(int rateLimitedPenalty) {
		this.rateLimitedPenalty = rateLimitedPenalty;
	}
}

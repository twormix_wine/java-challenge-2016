package org.twormix.types;

public class MoveSubmarineRequest extends AbstractGameRequest {
	
	private transient int submarineId;
	
	private double speed;
	private double turn;
	
	/**
	 * 
	 * @return the delta speed for next turn
	 */
	public double getSpeed() {
		return speed;
	}
	/**
	 * 
	 * @param speed the delta speed for next turn
	 */
	public void setSpeed(double speed) {
		this.speed = speed;
	}
	
	/**
	 * 
	 * @return the delta turning angle for next turn
	 */
	public double getTurn() {
		return turn;
	}
	
	/**
	 * 
	 * @param turn the delta turning angle for next turn
	 */
	public void setTurn(double turn) {
		this.turn = turn;
	}
	
	public int getSubmarineId() {
		return submarineId;
	}
	
	public void setSubmarineId(int submarineId) {
		this.submarineId = submarineId;
	}
}

package org.twormix.types;

import org.twormix.exceptional.TwormixException;

public abstract class AbstractResponse {
	
	public enum ErrorCode
	{
		GROUP_NOT_INVITED(1), 
		GAME_IN_PROGRESS(2), 
		NONEXISTENT_GAMEID(3),
		NO_RIGHT_TO_CONTROL_SUBMARINE(4),
		TORPEDO_ON_COOLDOWN(7),
		EXTENDED_SONAR_RECHARGING(8),
		GAME_IS_NOT_IN_PROGRESS(9),
		SUBMARINE_ALREADY_MOVED(10),
		TOO_MUCH_SPEED(11),
		TOO_MUCH_TURN(12),
		RATE_LIMITED(50);
		
		
		private ErrorCode(int code)
		{
			this.code = code;
		}
		
		private int code;
		
		public int getCode()
		{
			return code;
		}
		
		public static ErrorCode parseErrorCode(int code)
		{
			for (ErrorCode errc : ErrorCode.values())
			{
				if (errc.code == code)
					return errc;
			}
			throw new TwormixException("parseErrorCode");
		}
	}
	
	private String message;
	
	private int code;
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
}

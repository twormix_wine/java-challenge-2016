package org.twormix.types;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class Coordinate {
	private static final double DELTA = 0.0001;
	public double x;
	public double y;
	
	public Coordinate() {
		this(0,0);
	}
	
	public Coordinate(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(x);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(y);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Coordinate other = (Coordinate) obj;
		if (Math.abs(x - other.x) > DELTA)
			return false;
		if (Math.abs(y - other.y) > DELTA)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "[" + x + ", " + y + "]";
	}

	public static double distance(Coordinate a, Coordinate b) {
		double dist = 0.0;
		dist = (a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y);
		dist = Math.sqrt(dist);
		return dist;
	}

	public static Coordinate motion(Coordinate origin, double angleInDegrees, double velocity) {
		double radians = Math.toRadians(angleInDegrees);
		Coordinate destination = new Coordinate();
		destination.x = Math.cos(radians) * velocity + origin.x;
		destination.y = Math.sin(radians) * velocity + origin.y;
		return destination;
	}

	public static boolean isPointInBox(Coordinate point, Coordinate boxA, Coordinate boxB) {
		if (boxA.x < boxB.x) {
			if (point.x < boxA.x || point.x > boxB.x)
				return false;
		} else {
			if (point.x > boxA.x || point.x < boxB.x)
				return false;
		}

		if (boxA.y < boxB.y) {
			if (point.y < boxA.y || point.y > boxB.y)
				return false;
		} else {
			if (point.y > boxA.y || point.y < boxB.y)
				return false;
		}

		return true;
	}

	public static boolean isLineCrossesCircle(Coordinate origin, double radius, Coordinate start, Coordinate end) {
		return lineCircleCollisionPoints(origin, radius, start, end).size() > 0;
	}

	public static List<Coordinate> lineCircleCollisionPoints(Coordinate origin, double radius, Coordinate start,
			Coordinate end) {

		double dx, dy, A, B, C, det, t;
		dx = end.x - start.x;
		dy = end.y - start.y;

		A = dx * dx + dy * dy;
		B = 2 * (dx * (start.x - origin.x) + dy * (start.y - origin.y));
		C = (start.x - origin.x) * (start.x - origin.x) + (start.y - origin.y) * (start.y - origin.y) - radius * radius;

		det = B * B - 4 * A * C;
		if ((A <= DELTA) || (det < 0)) {
			return new ArrayList<>();
		} else if (det == 0) {
			Coordinate i1 = new Coordinate();
			t = -B / (2 * A);
			i1.x = start.x + t * dx;
			i1.y = start.y + t * dy;
			if (isPointInBox(i1, start, end)) {
				return Arrays.asList(i1);
			}
		} else {
			Coordinate i1 = new Coordinate();
			Coordinate i2 = new Coordinate();
			List<Coordinate> ret = new ArrayList<>();

			t = (-B + Math.sqrt(det)) / (2 * A);
			i1.x = start.x + t * dx;
			i1.y = start.y + t * dy;

			if (isPointInBox(i1, start, end))
				ret.add(i1);

			t = (-B - Math.sqrt(det)) / (2 * A);
			i2.x = start.x + t * dx;
			i2.y = start.y + t * dy;

			if (isPointInBox(i2, start, end))
				ret.add(i2);

			return ret;
		}

		return new ArrayList<>();
	}

	public static Coordinate add(Coordinate a, Coordinate b) {
		Coordinate ret = new Coordinate();
		ret.x = a.x + b.x;
		ret.y = a.y + b.y;
		return ret;
	}

	public static double normalizeDegrees(double degrees) {
		return degrees - 360 * ((int) Math.floor(degrees / 360));
	}

	public static double calcAngle(Coordinate start, Coordinate end) {

		double dx = end.x - start.x;
		double dy = end.y - start.y;
		
		if (dy == 0) {
			return dx < 0 ? 180 : 0;
		}
		
		if (dx == 0) {
			return dy < 0 ? 270 : 90;
		}
		
		double rad = Math.atan2(dy, dx);

		return normalizeDegrees(Math.toDegrees(rad));

	}
}

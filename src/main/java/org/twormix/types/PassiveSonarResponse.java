package org.twormix.types;

import java.util.List;

public class PassiveSonarResponse extends AbstractResponse {
	private List<Entity> entities;

	public List<Entity> getEntities() {
		return entities;
	}

	public void setEntities(List<Entity> entities) {
		this.entities = entities;
	}
}

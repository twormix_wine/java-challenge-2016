package org.twormix.types;

import java.util.List;

public class GetControlledSubmarinesResponse extends AbstractResponse {
	private List<Submarine> submarines;

	public List<Submarine> getSubmarines() {
		return submarines;
	}

	public void setSubmarines(List<Submarine> submarines) {
		this.submarines = submarines;
	}
}

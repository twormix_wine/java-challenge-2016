package org.twormix.types;

import java.util.List;

public class GameListResponse extends AbstractResponse {
	private List<Integer> games;

	public List<Integer> getGames() {
		return games;
	}

	public void setGames(List<Integer> games) {
		this.games = games;
	}
	
	
}

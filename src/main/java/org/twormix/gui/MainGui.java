package org.twormix.gui;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.util.List;

import javax.swing.*;

import org.twormix.game.logic.EnemyTeamPredicate;
import org.twormix.types.Coordinate;
import org.twormix.types.Submarine;
import org.twormix.types.Torpedo;

public class MainGui extends Canvas {

	private static final long serialVersionUID = 1L;

	private List<Coordinate> islands;
	private double r;
	private int mapWidth;
	private int mapHeight;
	private double subSize;
	private double torSize;

	private double sonarRange;
	private double sonarExtRange;

	private List<Submarine> submarines;
	private List<Torpedo> torpedos;

	private static Color dbrown = new Color(139, 69, 19);
	private static Color dgray = new Color(47, 79, 79);
	private static Color dred = new Color(139, 0, 0);
	private static Color dblack = new Color(0, 0, 0);

	EnemyTeamPredicate isEnemy = new EnemyTeamPredicate();

	public MainGui() {

	}

	@Override
	public void paint(Graphics g) {
		drawIsland(g);
		drawSubmarines(g);
		drawTorpedos(g);
	}

	private void drawSubmarines(Graphics g) {

		if (submarines == null) {
			return;
		}
		
		for (Submarine n : submarines) {
			double x = n.getPosition().x;
			double y = n.getPosition().y;
			double d = subSize * 2;

			x = x - subSize;
			y = mapHeight - y - subSize;

			boolean enemy = isEnemy.test(n);
			
			g.setColor(enemy ? dred : dgray);
			g.fillOval((int) x, (int) y, (int) d, (int) d);

			if (!enemy) {
				int sonarRadii = (int) sonarRange;
				if (n.getSonarExtended() > 0) {
					sonarRadii = (int) sonarExtRange;
				}

				double sonarX = n.getPosition().x - sonarRadii;
				double sonarY = mapHeight - n.getPosition().y - sonarRadii;
				double sonarD = 2 * sonarRadii;

				g.drawOval((int) sonarX, (int) sonarY, (int) sonarD, (int) sonarD);
			}
		}

	}

	private void drawTorpedos(Graphics g) {
		if (torpedos == null) {
			return;
		}
		for (Torpedo n : torpedos) {
			double x = n.getPosition().x;
			double y = n.getPosition().y;
			double d = torSize * 2;

			x = x - torSize;
			y = mapHeight - y - torSize;

			g.setColor(dblack);
			g.fillOval((int) x, (int) y, (int) d, (int) d);
		}

	}

	private void drawIsland(Graphics g) {
		if (islands == null) {
			return;
		}
		for (Coordinate n : islands) {
			int x = (int) n.x - (int) r;
			int y = (mapHeight - (int) n.y) - (int) r;
			int d = (int) r * 2;
			g.setColor(dbrown);

			g.fillOval(x, y, d, d);
		}
	}

	public void setMainGui(List<Coordinate> islands, double islandSize, int mapw, int maph) {
		this.islands = islands;
		r = islandSize;
		mapWidth = mapw;
		mapHeight = maph;
	}

	public void setSubmarines(List<Submarine> subs, double size) {
		submarines = subs;
		subSize = size;
		torSize = subSize / 2;
	}

	public void setTorpedos(List<Torpedo> tor) {
		torpedos = tor;
	}

	public void setSonar(double sonarRange, double sonarExtRange) {
		this.sonarRange = sonarRange;
		this.sonarExtRange = sonarExtRange;
	}

}

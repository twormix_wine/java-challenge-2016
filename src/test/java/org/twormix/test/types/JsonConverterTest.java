package org.twormix.test.types;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.twormix.types.ConnectionStatus;
import org.twormix.types.Coordinate;
import org.twormix.types.CreateGameResponse;
import org.twormix.types.Entity;
import org.twormix.types.Game;
import org.twormix.types.GameInfoResponse;
import org.twormix.types.GameListResponse;
import org.twormix.types.JoinGameRequest;
import org.twormix.types.MapConfiguration;
import org.twormix.types.PassiveSonarResponse;
import org.twormix.types.Scores;
import org.twormix.types.Submarine;
import org.twormix.types.Torpedo;
import org.twormix.types.json.EntityJsonConverter;
import org.twormix.types.json.EntityListJsonConverter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class JsonConverterTest {

	private static final double DELTA = 0.001;

	@Test
	public void testCreateGameResponseJsonConverting() {
		String jsonString = "{\"message\":\"OK\",\"code\":0,\"id\":1187580416}";

		Gson gson = new Gson();

		CreateGameResponse resp = new CreateGameResponse();
		resp.setMessage("OK");
		resp.setCode(0);
		resp.setId(1187580416);

		CreateGameResponse returned = gson.fromJson(jsonString, CreateGameResponse.class);
		Assert.assertNotNull(returned);
		Assert.assertNotNull(returned.getMessage());
		Assert.assertEquals(resp.getMessage(), returned.getMessage());
		Assert.assertEquals(resp.getCode(), returned.getCode());
		Assert.assertEquals(resp.getId(), returned.getId());

		String returnedString = gson.toJson(resp);
		CreateGameResponse returned2 = gson.fromJson(returnedString, CreateGameResponse.class);
		Assert.assertNotNull(returned2);
		Assert.assertNotNull(returned2.getMessage());
		Assert.assertEquals(resp.getMessage(), returned2.getMessage());
		Assert.assertEquals(resp.getCode(), returned2.getCode());
		Assert.assertEquals(resp.getId(), returned2.getId());
	}

	@Test
	public void testGameListResponseJsonConverting() {
		String jsonString = "{\"games\":[1725277523,584929124,2062147061],\"message\":\"OK\",\"code\":0}";

		List<Integer> games = new ArrayList<>();
		games.add(1725277523);
		games.add(584929124);
		games.add(2062147061);

		Gson gson = new Gson();

		GameListResponse resp = new GameListResponse();
		resp.setGames(games);
		resp.setMessage("OK");
		resp.setCode(0);

		GameListResponse returned = gson.fromJson(jsonString, GameListResponse.class);
		Assert.assertNotNull(returned);
		Assert.assertNotNull(returned.getMessage());
		Assert.assertNotNull(returned.getGames());
		Assert.assertEquals(resp.getMessage(), returned.getMessage());
		Assert.assertEquals(resp.getCode(), returned.getCode());
		Assert.assertEquals(returned.getGames().size(), resp.getGames().size());
		for (int i : resp.getGames())
			Assert.assertTrue(returned.getGames().contains(i));

		String returnedString = gson.toJson(resp);
		GameListResponse returned2 = gson.fromJson(returnedString, GameListResponse.class);
		Assert.assertNotNull(returned2);
		Assert.assertNotNull(returned2.getMessage());
		Assert.assertNotNull(returned2.getGames());
		Assert.assertEquals(resp.getMessage(), returned2.getMessage());
		Assert.assertEquals(resp.getCode(), returned2.getCode());
		for (int i : resp.getGames())
			Assert.assertTrue(returned2.getGames().contains(i));
	}

	@Test
	public void testJoinGameRequestJsonConverting() {
		String jsonString = "{}";

		Gson gson = new Gson();

		JoinGameRequest req = new JoinGameRequest();
		req.setGameId(1234);

		JoinGameRequest ret = gson.fromJson(jsonString, JoinGameRequest.class);
		Assert.assertNotNull(ret);
		String jsonStr = gson.toJson(req);
		Assert.assertEquals(jsonString, jsonStr);
	}

	@Test
	public void testGameInfoResponseJsonConverting() {
		String jsonString = "{\"game\":{" + "\"id\":405034534," + "\"round\":0," + "\"scores\":{" + "\"scores\":{"
				+ "\"BOT\":0," + "\"Team 1\":0}}," + "\"connectionStatus\":{" + "\"connected\":{" + "\"BOT\": true,"
				+ "\"Team 1\":false}}," + "\"mapConfiguration\":{" + "\"width\": 1700," + "\"height\": 800,"
				+ "\"islandPositions\":[{" + "\"x\":400," + "\"y\":400},{" + "\"x\":1000," + "\"y\":700},{"
				+ "\"x\":1600," + "\"y\":300}]," + "\"teamCount\":1," + "\"submarinesPerTeam\":3,"
				+ "\"torpedoDamage\":34," + "\"torpedoHitScore\":100," + "\"torpedoDestroyScore\":50,"
				+ "\"torpedoHitPenalty\":50," + "\"torpedoCooldown\":3," + "\"sonarRange\":100,"
				+ "\"extendedSonarRange\":200," + "\"extendedSonarRounds\":10," + "\"extendedSonarCooldown\":20,"
				+ "\"torpedoSpeed\":20," + "\"torpedoExplosionRadius\":100," + "\"roundLength\":2000,"
				+ "\"islandSize\":100," + "\"submarineSize\":15," + "\"rounds\":300," + "\"maxSteeringPerRound\":10,"
				+ "\"maxAccelerationPerRound\":5," + "\"maxSpeed\":15," + "\"torpedoRange\":10,"
				+ "\"rateLimitedPenalty\":10}," + "\"status\":\"WAITING\"}," + "\"message\":\"OK\"," + "\"code\": 0}";

		Gson gson = new Gson();

		GameInfoResponse response = gson.fromJson(jsonString, GameInfoResponse.class);
		Assert.assertNotNull(response);
		Assert.assertNotNull(response.getMessage());
		Assert.assertNotNull(response.getGame());
		Game game = response.getGame();
		Assert.assertNotNull(game.getStatus());
		Assert.assertNotNull(game.getConnectionStatus());
		ConnectionStatus status = game.getConnectionStatus();
		Assert.assertNotNull(status.getConnected());
		Assert.assertNotNull(game.getScores());
		Scores scores = game.getScores();
		Assert.assertNotNull(scores.getScores());
		Assert.assertNotNull(game.getMapConfiguration());
		MapConfiguration mapConfig = game.getMapConfiguration();
		Assert.assertNotNull(mapConfig.getIslandPositions());

		Assert.assertEquals(response.getMessage(), "OK");
		Assert.assertEquals(response.getCode(), 0);
		Assert.assertEquals(game.getId(), 405034534);
		Assert.assertEquals(game.getRound(), 0);
		Assert.assertEquals(game.getStatus(), "WAITING");
		Assert.assertEquals(status.getConnected().keySet().size(), 2);
		Assert.assertEquals(status.getConnected().get("BOT"), true);
		Assert.assertEquals(status.getConnected().get("Team 1"), false);
		Assert.assertEquals(scores.getScores().keySet().size(), 2);
		Assert.assertEquals(scores.getScores().get("BOT"), Integer.valueOf(0));
		Assert.assertEquals(scores.getScores().get("Team 1"), Integer.valueOf(0));
		Assert.assertEquals(mapConfig.getIslandPositions().size(), 3);
		Coordinate[] coords = { new Coordinate(), new Coordinate(), new Coordinate() };
		coords[0].x = coords[0].y = 400;
		coords[1].x = 1000;
		coords[1].y = 700;
		coords[2].x = 1600;
		coords[2].y = 300;
		for (Coordinate c : coords) {
			Assert.assertTrue(mapConfig.getIslandPositions().contains(c));
		}
		Assert.assertEquals(mapConfig.getWidth(), 1700);
		Assert.assertEquals(mapConfig.getHeight(), 800);
		Assert.assertEquals(mapConfig.getTeamCount(), 1);
		Assert.assertEquals(mapConfig.getSubmarinesPerTeam(), 3);
		Assert.assertEquals(mapConfig.getTorpedoDamage(), 34);
		Assert.assertEquals(mapConfig.getTorpedoHitScore(), 100);
		Assert.assertEquals(mapConfig.getTorpedoDestroyScore(), 50);
		Assert.assertEquals(mapConfig.getTorpedoHitPenalty(), 50);
		Assert.assertEquals(mapConfig.getTorpedoCooldown(), 3);
		Assert.assertEquals(mapConfig.getSonarRange(), 100, DELTA);
		Assert.assertEquals(mapConfig.getExtendedSonarRange(), 200, DELTA);
		Assert.assertEquals(mapConfig.getExtendedSonarRounds(), 10);
		Assert.assertEquals(mapConfig.getExtendedSonarCooldown(), 20);
		Assert.assertEquals(mapConfig.getTorpedoSpeed(), 20, DELTA);
		Assert.assertEquals(mapConfig.getTorpedoExplosionRadius(), 100, DELTA);
		Assert.assertEquals(mapConfig.getRoundLength(), 2000);
		Assert.assertEquals(mapConfig.getIslandSize(), 100, DELTA);
		Assert.assertEquals(mapConfig.getSubmarineSize(), 15, DELTA);
		Assert.assertEquals(mapConfig.getRounds(), 300);
		Assert.assertEquals(mapConfig.getMaxSteeringPerRound(), 10, DELTA);
		Assert.assertEquals(mapConfig.getMaxAccelerationPerRound(), 5, DELTA);
		Assert.assertEquals(mapConfig.getMaxSpeed(), 15, DELTA);
		Assert.assertEquals(mapConfig.getTorpedoRange(), 10, DELTA);
		Assert.assertEquals(mapConfig.getRateLimitedPenalty(), 10);
	}

	@Test
	public void testEntityTypeAdapter() {
		final String torpedo = "{\"type\": \"Torpedo\",\"id\": 1136,"
				+ "\"position\": {\"x\": 623.9475810178445,\"y\": 165.1967719691951},"
				+ "\"owner\": {\"name\": \"Team 4\"},\"velocity\": 40,\"angle\": 358.6153869628906,\"roundsMoved\": 6}";
		final String submarine = "{\"type\": \"Submarine\",\"id\": 635," + "\"position\": {\"x\": 200,\"y\": 200},"
				+ "\"owner\": {\"name\": \"Team 1\"},\"velocity\": 0,\"angle\": 0}";
		GsonBuilder builder = new GsonBuilder();
		builder.registerTypeAdapter(Entity.class, new EntityJsonConverter());
		Gson gson = builder.create();
		Entity t = gson.fromJson(torpedo, Entity.class);
		Entity s = gson.fromJson(submarine, Entity.class);

		Assert.assertEquals(Submarine.class, s.getClass());
		Assert.assertEquals(Torpedo.class, t.getClass());
	}

	@Test
	public void testSonarResponseJsonConverting() {
		String jsonString = "{\"entities\": " + "[{\"type\": \"Submarine\",\"id\": 635,"
				+ "\"position\": {\"x\": 200,\"y\": 200},"
				+ "\"owner\": {\"name\": \"Team 1\"},\"velocity\": 0,\"angle\": 0},"
				+ "{\"type\": \"Torpedo\",\"id\": 1136,"
				+ "\"position\": {\"x\": 623.9475810178445,\"y\": 165.1967719691951},"
				+ "\"owner\": {\"name\": \"Team 4\"},\"velocity\": 40,\"angle\": 358.6153869628906,\"roundsMoved\": 6}],"
				+ "\"message\": \"OK\",\"code\": 0}";

		GsonBuilder builder = new GsonBuilder();
		builder.registerTypeAdapter(Entity.class, new EntityJsonConverter());
		builder.registerTypeAdapter(new ArrayList<Entity>().getClass(), new EntityListJsonConverter());
		Gson gson = builder.create();

		PassiveSonarResponse ret = gson.fromJson(jsonString, PassiveSonarResponse.class);
		Assert.assertNotNull(ret);
		Assert.assertNotNull(ret.getEntities());
		Assert.assertEquals(ret.getEntities().size(), 2);
		Assert.assertEquals(Submarine.class, ret.getEntities().get(0).getClass());
		Assert.assertEquals(Torpedo.class, ret.getEntities().get(1).getClass());
	}
}

package org.twormix.test.math;

import java.util.List;

import org.junit.Test;
import org.twormix.types.Coordinate;

import junit.framework.Assert;

public class MathCalcTest {

	@Test
	public void testAngleCalculator() {

		Coordinate a, b;
		a = new Coordinate();
		b = new Coordinate();
		a.x = 1;
		a.y = 1;
		b.x = 0;
		b.y = 0;
		Assert.assertEquals(45, Coordinate.calcAngle(b, a), 1);
		a.x = -1;
		a.y = 1;
		Assert.assertEquals(135, Coordinate.calcAngle(b, a), 1);
		a.x = -1;
		a.y = 0;
		Assert.assertEquals(180, Coordinate.calcAngle(b, a), 1);
		a.x = 1;
		a.y = 0;
		Assert.assertEquals(0, Coordinate.calcAngle(b, a), 1);
		a.x = 0;
		a.y = 1;
		Assert.assertEquals(90, Coordinate.calcAngle(b, a), 1);
		a.x = 0;
		a.y = -1;
		Assert.assertEquals(270, Coordinate.calcAngle(b, a), 1);
	}

	@Test
	public void testCollisionPointCalculator() {

		Coordinate origin = new Coordinate(1, 1);
		double radii = 3;
		Coordinate a = new Coordinate(-3, 2.7);
		Coordinate b = new Coordinate(10, 2);
		Coordinate c = new Coordinate(3, 100);
		Coordinate d = new Coordinate(1, 2);
		List<Coordinate> r = Coordinate.lineCircleCollisionPoints(origin, radii, a, b);
		List<Coordinate> r2 = Coordinate.lineCircleCollisionPoints(origin, radii, a, c);
		List<Coordinate> r3 = Coordinate.lineCircleCollisionPoints(origin, radii, a, d);
		Assert.assertEquals(2, r.size());
		Assert.assertEquals(0, r2.size());
		Assert.assertEquals(1, r3.size());
	}

}
